# vueda

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### vue-admin项目

#### 项目准备
* 通过gitlab创建远程仓库
* 初始化vue项目
* 删除vue项目中没用代码，文件
* 创建dev开发分支
* 基于dev分支创建特征分支开发（feature_*）

#### 开始开发
* 去除默认样式 reset-css
* 安装element-ui

``` 
    写后台管理系统项目:
    ui框架选择：
    1、element-ui（饿了么）
    2、iview（个人）
    3、antd vue（支付宝）
    移动端：
    vant
    weui
    zanui
```
#### login模块

* ajax封装

1、项目开发是分为不同的环境的，所以要考虑接口基准地址是要频繁切换的。（baseUrl切换问题）
2、所有请求抽离出来，放到单独的js文件内，方便后期维护
3、成功处理或者错误处理，都可以统一处理了
4、拦截器
* 表单验证

* 登录状态存储
* 路由守卫

#### home页面

* 页面布局（布局容器）
* 左侧菜单折叠效果

```
* 菜单index属性必须绑定唯一值。
* 退出登录
 1、删除登录状态（vuex，localstorage登录状态清除就可以了），跳转登录页
 2、调用退出接口，删除登录状态，跳转登录页
 3、权限菜单关联路由
 4、动态添加路由（addRoutes）
```

**** 问题：拦截器，折叠面板 

#### 添加商品
* 上传图片注意事项
1、设置请求头
2、参数名称默认file
3、参数需要new Formdata()

#### nginx基础

