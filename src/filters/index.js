import Vue from 'vue'
import dayjs from 'dayjs'
Vue.filter('formateDate', (val) =>{
    return dayjs(val).format('YYYY-MM-DD HH:MM:ss')
})