import http from './index'
import { Message } from 'element-ui'
//登录
export const login = async (obj) => {
        const res = await http('login', 'POST', {}, obj)
        Message({
            type: 'success',
            message: res.meta.msg
        })
        return res;
}

//获取权限菜单表
export const getMenus = async (route) => {
    const res = await http('menus', 'GET', {},{},route)
    return res.data;
}

//获取用户列表

export const getUsers = async (params) => {
    const res = await http('users', 'GET', params)
    return res.data;
}
//修改用户状态

export const changeUserStatus = async (obj) => {
    const res = await http(`users/${obj.id}/state/${obj.type}`, 'PUT')
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}

//查詢用戶信息

export const getUserInfo = async (id) => {
    const res = await http(`users/${id}`, 'GET')
    return res.data;
}


//獲取角色列表 

export const getRolesLists = async () => {
    const res = await http(`roles`, 'GET')
    return res.data;
}

//分配用户角色
export const distributeRole = async (obj) => {
    const res = await http(`users/${obj.id}/role`, 'PUT' , {}, {
        rid: obj.rid
    })
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}


//获取商品列表
export const getGoodsLists = async (params) => {
    const res = await http('goods', 'GET', params)
    return res.data;
}

//添加角色
export const addRole = async (obj) => {
    const res = await http('roles', 'POST', {}, obj)
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}
//编辑角色
export const editRole = async (obj) => {
    const res = await http(`roles/${obj.roleId}`, 'PUT', {}, obj)
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}
//删除角色指定权限
export const delRoleRights = async (obj) => {
    const res = await http(`roles/${obj.roleId}/rights/${obj.rightId}`, 'DELETE')
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}

//删除角色
export const delRole = async (id) => {
    const res = await http(`roles/${id}`, 'DELETE')
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}

//获取所有权限
export const getRightsByTree = async () => {
    const res = await http(`rights/tree`, 'GET')
    return res.data;
}


//授权
export const deliverRole = async (obj) => {
    const res = await http(`roles/${obj.roleId}/rights`, 'POST', {}, obj)
    Message({
        type: 'success',
        message: res.meta.msg
    })
    return res.data;
}



//获取分类列表

export const getCategories = async () => {
    const res = await http(`categories`, 'GET', {
        type: 3
    })
    return res.data;
}

//获取商品参数
export const getCategoriesAttrs = async (obj) => {
    const res = await http(`categories/${obj.id}/attributes`, 'GET', {
        sel: obj.sel
    })
    return res.data;
}

