import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import 'reset-css'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import { Table } from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import http from '@/http'
import '@/filters'
import VueDraggable from 'vue-draggable'
Vue.use(VueDraggable)
import VueQuillEditor from 'vue-quill-editor'

import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme
Vue.use(VueQuillEditor, /* { default global options } */)
import animated from 'animate.css' // npm install animate.css --save安装，在引入
Vue.use(animated)
Vue.use(Table);
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$http = http

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
