import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/users',
    children: [
      {
        path: 'users',
        name: 'Users',
        meta: {
          title: '用户列表'
        },
        component: () => import('@/views/users/Index.vue')
      },
      {
        path: 'roles',
        name: 'Roles',
        meta: {
          title: '角色列表'
        },
        component: () => import('@/views/rights/Roles.vue')
      }, {
        path: 'rights',
        name: 'Rights',
        meta: {
          title: '权限列表'
        },
        component: () => import('@/views/rights/Index.vue')
      },
      {
        path: 'goods',
        name: 'Goods',
        meta: {
          title: '商品列表'
        },
        component: () => import('@/views/goods/Index.vue')
      },
      {
        path: 'addgoods',
        name: 'AddGoods',
        meta: {
          title: '添加商品'
        },
        component: () => import('@/views/goods/AddGoods.vue')
      }
      
    ]
  },{
    path: '/login',
    name: 'Login',
    meta: {
      notAuthorization: true
    },
    component: () => import('@/views/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: "/vue-admin/",
  routes
})

router.beforeEach((to,from,next) => {
  if (to.name !== 'Login' && !store.state.token) next({ name: 'Login',query: {
    redirect: to.name
  } })
  else next()
})

export default router
