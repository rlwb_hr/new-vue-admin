import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: localStorage.getItem('token')||'',
    username: localStorage.getItem('username') || ''
  },
  mutations: {
    // HANDLE_USERNAME(state, data){
    //   state.username = data.username
    //   localStorage.setItem('username', data.username)
    // },
    // HANDLE_TOKEN(state, data) {
    //   state.token = data.token
    //   localStorage.setItem('token', data.token)
    // },
    HANDLE_STATE(state, data) {
      for(let key in data){
        state[key] = data[key]
        localStorage.setItem([key],data[key])
      }
      // let arr = Object.keys(data)
      // state[arr[0]] = data[arr[0]]
      // localStorage.setItem([arr[0]], data[arr[0]])
    }
  },
  actions: {
  },
  modules: {
  }
})
